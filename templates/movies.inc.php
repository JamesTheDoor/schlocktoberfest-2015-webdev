<div class="row">
	<div class="col-xs-12">
		<h1>Movies</h1>
		<ol class="breadcrumb">
		  <li><a href=".\">Home</a></li>
		  <li  class="active">Movies</li>
		</ol>
		<ul>
		<?php
			foreach($movies as $movie): ?>
				<li><a href=".\?page=movie&amp;id=<?= $movie->id?>"><?= $movie->title; ?> (<?= $movie->year?>)</a></li>
				
			<?php endforeach; ?>
		</ul>
		
	</div>
	
</div>