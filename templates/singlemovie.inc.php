<div class="row">
	<div class="col-xs-12">
		<ol class="breadcrumb">
		  <li><a href=".\">Home</a></li>
		  <li><a href=".\?page=movies">Movies</a></li>
		  <li class="active"><?= $movie->title; ?></li>
		</ol>
				
			
		<h2><?= $movie->title; ?></h2>
		<p>Released in <?= $movie->year; ?></p>
		<p><?= $movie->description; ?></p>
		
		
	</div>
	
</div>