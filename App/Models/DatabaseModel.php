<?php

namespace App\Models;

use PDO;
use UnexpectedValueException;

abstract class DatabaseModel
{
	public $data;
	private static $db;

	public function __construct($id = null)
	{
		if(is_integer($id) && $id > 0 ){
			$this->find($id);
		}
	}

	private static function getDatabaseConnection() 
	{
		if (! self::$db){
			$dsn = 'mysql:host=localhost;dbname=schlocktoberfest;charset=utf8';
			self::$db = new PDO($dsn, 'root', '');

			self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
		return self::$db;
	
	}
	public static function all($sortcolumn = "", $asc = true)
	{
		$models = [];

		$db = static::getDatabaseConnection();

		$query ="SELECT " .implode("," , static::$columns). " FROM " . static::$tableName;

		if($sortcolumn){
			if( ! array_search($sortcolumn, static::$columns)){
				throw new UnexpectedValueException("Property $sortcolumn is not found in the columns array.");
			}
			$query .= " ORDER BY " .$sortcolumn;
			if($asc){
				$query .= " ASC";
			} else {
				$query .= " DESC";
			}
		}
		$statement = $db->prepare($query);
		$statement->execute();

		while($record = $statement->fetch(PDO::FETCH_ASSOC)){
			$model = new static();
			$model->data = $record;
			array_push($models, $model);

		}
		
		return $models;
		// var_dump($models);
	}
	public function find($id) 
	{
		$db = static::getDatabaseConnection();
		$query ="SELECT " .implode("," , static::$columns). " FROM " . static::$tableName . 
			" WHERE id = :id ";
		$statement = $db->prepare($query);
		$statement->bindValue(":id", $id);
		$statement->execute();

		while($record = $statement->fetch(PDO::FETCH_ASSOC)) {
			$this->data = $record;
		}
	}
	public function __get($name)
	{
		if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        throw new UnexpectedValueException("Property $name not found in the data variable.");
        
	}
}