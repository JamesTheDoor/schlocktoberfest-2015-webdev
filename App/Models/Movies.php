<?php

namespace App\Models;

class Movies extends DatabaseModel
{
	
	protected static $tableName = "movies";
	protected static $columns = ['id','title','year','description'];
	
}