<?php

namespace App\Controllers;

use App\Views\MoviesView;
use App\Models\Movies;
use App\Views\SingleMovieView;

class MoviesController 
{
	public function index()
	{
		$movies = Movies::all("title");
		$view = new MoviesView(['movies' => $movies]);
		$view->render();
	}
	public function show()
	{
		$movie = new Movies((int)$_GET['id']);
		$view = new SingleMovieView(['movie' => $movie]);
		$view->render();
	}
}