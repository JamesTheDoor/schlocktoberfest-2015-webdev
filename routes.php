<?php

namespace App\Controllers;

$page = isset($_GET['page']) ? $_GET['page'] : 'home';

switch ($page) {
	case "home":
		
		$controller = new HomeController();
		$controller->show();

		break;
	case "movies":

		$controller = new MoviesController();
		$controller->index();

		break;
	case "movie":

		$controller = new MoviesController();
		$controller->show();

		break;
	case "merchandise":

		$controller = new MerchandiseController();
		$controller->show();

	break;

	case "about":
		$controller = new AboutController();
		$controller->show();

		break;

	case "moviesuggest":

		$controller = new MovieSuggestController();
		$controller->show();		
		
		break;
		
	default:
		echo "404";
		
}













